.. _intro:

Introduction
============

.. image:: img/PocketMine.png
   :align: center


PocketMine-MP is a server software for Minecraft: Pocket Edition.
It has a Plugin API that enables developers
to add new features or change default ones.

Supported features
------------------

* Get all your friends in one server. Or un a public server.
* Disables flying, item hacks, running and more. With an On/Off switch.
* Extend the game in the way you want, add awesome features.
* Teleport players, whitelist your server, tune the server.
* Load different levels at once, and teleport back and forth.
* Endless features, and we continuously implement new things.


.. include:: contact.rst
