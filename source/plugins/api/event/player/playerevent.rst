PlayerDeathEvent
""""""""""""""""

This event happens when a player dies.

Functions
'''''''''

getEntity()
    returns Player object

getDeathMessage()
    returns death message (string)

setDeathMessage($deathMessage)
    set deathMessage (string)

getKeepInventory()
    returns true or false (bool)

setKeepInventory($keepInventory)
    set KeepInventory (bool)

Example
'''''''

.. code-block:: php

   use pocketmine\Player;
   use pocketmine\event\player\PlayerDeathEvent;

    /**
     * @param PlayerDeathEvent $event
     *
     * @priority LOWEST
     */
    public function onPlayerDeath(PlayerDeathEvent $event){
        if($event->getEntity() instanceof Player){
            // code
        }
    }
