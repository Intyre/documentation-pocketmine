Player events
~~~~~~~~~~~~~

.. toctree::
    :glob:
    :maxdepth: 1

    player/*
