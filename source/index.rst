.. image:: img/PocketMine.png
   :align: center

########################
PocketMine Documentation
########################

.. toctree::
   :maxdepth: 2

   intro
   requirements
   installation
   configuration
   faq
   plugins
   contact
   links



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

