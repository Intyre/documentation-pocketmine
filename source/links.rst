Links
-----

PocketMine-MP
~~~~~~~~~~~~~

* `PocketMine Website <https://www.pocketmine.net>`_
* `PocketMine Forums <https://forums.pocketmine.net/>`_
* `PocketMine Jenkins <http://jenkins.pocketmine.net/>`_
* `PocketMine GitHub <https://www.github.com/PocketMine/>`_
* `PocketMine-MP GitHub <https://github.com/PocketMine/PocketMine-MP>`_
* `PocketMine-MP Translation <http://translate.pocketmine.net/>`_
* `PocketMine-MP Documentation <http://pocketmine-mp.readthedocs.org/en/latest/>`_
* `PocketMine-MP API Documentation <http://docs.pocketmine.net/>`_
