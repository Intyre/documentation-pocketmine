Can I use .php files
""""""""""""""""""""

Yes, 
but only when the `DevTools`_ plugin is installed and the plugin/PocketMine API versions are both the same.

.. _DevTools: http://forums.pocketmine.net/plugins/devtools.515/
