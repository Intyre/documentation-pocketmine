How do I connect to a server
""""""""""""""""""""""""""""

* Tap Play -> Edit -> External, then fill in the server details.

.. note::
    A local server should show up on the Play screen without adding the details.
