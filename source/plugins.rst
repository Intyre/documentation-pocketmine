Plugins
=======

**PocketMine-MP is extendable!**

Installing plugins
------------------

Creating a plugin
-----------------

Using the API
-------------

.. toctree::
    :glob:
    :maxdepth: 2

    plugins/api/event/*

