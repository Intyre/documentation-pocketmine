.. _requirements:

Requirements
============

PocketMine-MP.phar, the main component, is needed to run PocketMine-MP.
PocketMine-MP requires a few extra extensions because of that you need a custom build PHP binary.
We precompiled a few binaries. 
If you have problems, you could try to compile PHP with our `compile script`_.
Got problems? :doc:`contact`.

Download links
--------------

* `Windows installer`_
* `Linux/MacOS install script`_
* `Linux/MacOS binaries`_
* PocketMine-MP.phar (`Jenkins`_, `GitHub`_)

.. _compile script: https://github.com/PocketMine/php-build-scripts/blob/master/compile.sh
.. _Windows installer: http://sourceforge.net/projects/pocketmine/files/windows/dev/
.. _Linux/MacOS install script: http://get.pocketmine.net/
.. _Linux/MacOS binaries: http://sourceforge.net/projects/pocketmine/files/builds/
.. _Jenkins: http://jenkins.pocketmine.net/job/PocketMine-MP/promotion/
.. _GitHub: https://github.com/PocketMine/PocketMine-MP/releases
